﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Text;
using ShippingAPI.Libraries;
using System.Runtime.Serialization.Json;
using System.Diagnostics.Contracts;
using ShippingAPI.Models;
using ShippingAPI.Filters;
using System.Web.Script.Serialization;
using ShippingAPI.Responses;
using ShippingAPI.Requests;

namespace ShippingAPI.Controllers
{
    public class ApiController : Controller
    {
        //https://gist.github.com/leggetter/769688
        //https://github.com/ranacseruet/webrequest-csharp/blob/master/MyWebRequest.cs

        public object ApiJsonResponse { get; private set; }
        private string responseText { get; set; }

        private CatchWebResponse webResponse = new CatchWebResponse();
        private ShippingMethods shipMethods = new ShippingMethods();
        private UPSResponses upsResponses = new UPSResponses();
        private FedExResponses fedexResponses = new FedExResponses();
        private UPSRequest upsRequest = new UPSRequest();

        [Route("api/rates/{company}")]
        public JsonResult Rate(string company) {
            webResponse.setWebRequest();
            var myRateResponse = JsonConvert.DeserializeObject<CatchRateResponse>(webResponse.getWebRequest());


            if(company == null) {
                throw new Exception("Please provide a shipping Company");
            }

            try
            {
                var ratingResponse = new object();
                switch (company)
                {
                    case "ups":
                        upsResponses.GrabRateResponse(myRateResponse);
                        ratingResponse = upsResponses.RateJSONResponse();
                        break;

                    case "fedex":
                        FedEx fedexAPI = new FedEx(myRateResponse.credentials);
                        fedexAPI.generateRates();
                        ratingResponse = fedexResponses.RateJSONResponse(fedexAPI.getRateResponse());
                        break;
                }

                ApiJsonResponse = ratingResponse;


            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {

                ApiJsonResponse = new
                {
                    response = "error",
                    message = ex.Message
                };
                Console.WriteLine("SoapException Message= " + ex.Message);
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                ApiJsonResponse = new
                {
                    response = "error",
                    message = ex.Message
                };
                Console.WriteLine("CommunicationException= " + ex.Message);
            }
            catch (WebException ex)
            {
                ApiJsonResponse = new
                {
                    response = "error",
                    message = ex.Message
                };

                Console.WriteLine(" Web Exception= " + ex.Message);
                Console.WriteLine(" Web Exception-StackTrace= " + ex.StackTrace);
            }
            catch (System.Exception ex)
            {
                ApiJsonResponse = new
                {
                    response = "error",
                    message = ex.Message
                };
                //Console.WriteLine("");
                //Console.WriteLine("-------------------------");
                Console.WriteLine(" General Exception= " + ex.Message);
                Console.WriteLine(" General Exception-StackTrace= " + ex.StackTrace);
                //Console.WriteLine("-------------------------");

            }

           
            finally
            {
                Console.ReadKey();
            }

            return Json(ApiJsonResponse, JsonRequestBehavior.AllowGet);

        }

    }
}
