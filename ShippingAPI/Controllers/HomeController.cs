﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using ShippingAPI.UPSRate;
using ShippingAPI.UPSAddressValidation;
using System.ServiceModel;
using System.Diagnostics.Contracts;
using System.Xml.Linq;
using ShippingAPI.Properties.UPS;
using System.Text;

namespace ShippingAPI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var mvcName = typeof(Controller).Assembly.GetName();
            var isMono = Type.GetType("Mono.Runtime") != null;

            ViewData["Version"] = mvcName.Version.Major + "." + mvcName.Version.Minor;
            ViewData["Runtime"] = isMono ? "Mono" : ".NET";

            return View();
        }

        public ActionResult RateTest() {

            try
            {
                //testing for ups rate api
                RateService rate = new RateService();
                RateRequest rateRequest = new RateRequest();

                UPSSecurity upss = new UPSSecurity();
                UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
                upssSvcAccessToken.AccessLicenseNumber = "1CEB3632E5A4A196";
                upss.ServiceAccessToken = upssSvcAccessToken;
                UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
                upssUsrNameToken.Username = "crazy8monkey";
                upssUsrNameToken.Password = "Crazy3@monkey";
                upss.UsernameToken = upssUsrNameToken;
                rate.UPSSecurityValue = upss;

                UPSRequestType request = new UPSRequestType();

                //String[] requestOption = { "Shop" };
                //String[] requestOption = { "Shoptimeintransit" };
                String[] requestOption = { "Ratetimeintransit" };

                request.RequestOption = requestOption;

                //rateRequest.Shipment.
                rateRequest.Request = request;



                ShipmentType shipment = new ShipmentType();
                ShipperType shipper = new ShipperType();
                shipper.ShipperNumber = "050E34";


                TimeInTransitRequestType deliveryTime = new TimeInTransitRequestType();
                deliveryTime.PackageBillType = "02";

                PickupType pickup = new PickupType();
                pickup.Date = "20221228";
                pickup.Time = "1140";

                deliveryTime.Pickup = pickup;
                shipment.DeliveryTimeInformation = deliveryTime;


                ShippingAPI.UPSRate.AddressType shipperAddress = new ShippingAPI.UPSRate.AddressType();
                String[] addressLine = { "5555 main", "4 Case Cour", "Apt 3B" };
                shipperAddress.AddressLine = addressLine;
                shipperAddress.City = "Roswell";
                shipperAddress.PostalCode = "30076";
                shipperAddress.StateProvinceCode = "GA";
                shipperAddress.CountryCode = "US";
                shipperAddress.AddressLine = addressLine;
                shipper.Address = shipperAddress;
                shipment.Shipper = shipper;

                ShipFromType shipFrom = new ShipFromType();
                ShippingAPI.UPSRate.AddressType shipFromAddress = new ShippingAPI.UPSRate.AddressType();
                shipFromAddress.AddressLine = addressLine;
                shipFromAddress.City = "Roswell";
                shipFromAddress.PostalCode = "30076";
                shipFromAddress.StateProvinceCode = "GA";
                shipFromAddress.CountryCode = "US";
                shipFrom.Address = shipFromAddress;
                shipment.ShipFrom = shipFrom;

                ShipToType shipTo = new ShipToType();
                ShipToAddressType shipToAddress = new ShipToAddressType();
                String[] addressLine1 = { "10 E. Ritchie Way", "2", "Apt 3B" };
                shipToAddress.AddressLine = addressLine1;
                shipToAddress.City = "Plam Springs";
                shipToAddress.PostalCode = "92262";
                shipToAddress.StateProvinceCode = "CA";
                shipToAddress.CountryCode = "US";
                shipTo.Address = shipToAddress;
                shipment.ShipTo = shipTo;

                string shippingMethodString = "02";

                RateCodeDescriptionType service = new RateCodeDescriptionType();
                //Below code uses dummy date for reference. Please udpate as required.
                service.Code = "59";
                shipment.Service = service;

                //shipment.

                //DeliveryConfirmationType delivery = new DeliveryConfirmationType();
                //delivery.DCISType = "1";

                RateCodeDescriptionType uom = new RateCodeDescriptionType();
                uom.Code = "LBS";
                uom.Description = "pounds";

                PackageWeightType packageWeight = new PackageWeightType();
                packageWeight.Weight = "15";
                packageWeight.UnitOfMeasurement = uom;

                RateCodeDescriptionType packType = new RateCodeDescriptionType();
                packType.Code = shippingMethodString;

                PackageType package = new PackageType();
                package.PackageWeight = packageWeight;
                package.PackagingType = packType;



                List<PackageType> pkgArray = new List<PackageType>();
                //PackageType[] pkgArray = { package };
                //PackageType[] pkgArray = new PackageType[];
                pkgArray.Add(package);
               

                shipment.Package = pkgArray.ToArray();





                rateRequest.Shipment = shipment;
                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                //Console.WriteLine(rateRequest);
                RateResponse rateResponse = rate.ProcessRate(rateRequest);
                //Console.WriteLine("The transaction was a " + rateResponse.Response.ResponseStatus.Description);
                //Console.WriteLine("Total Shipment Charges " + rateResponse.RatedShipment[0].TotalCharges.MonetaryValue + rateResponse.RatedShipment[0].TotalCharges.CurrencyCode);
                //Console.ReadKey();
                //rateResponse.Response

                var ratesTestString = new StringBuilder();

                foreach(var rates in rateResponse.RatedShipment) {
                    ratesTestString.AppendFormat("<div>{0} - {1}</div>", rates.TotalCharges.MonetaryValue, rates.Service.Code);
                }

                //ViewData["Response"] = rateResponse.RatedShipment[0].TotalCharges.MonetaryValue + rateResponse.RatedShipment[0].TotalCharges.CurrencyCode;
                ViewData["Response"] = ratesTestString.ToString();
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                Console.WriteLine("");
                Console.WriteLine("---------Rate Web Service returns error----------------");
                Console.WriteLine("---------\"Hard\" is user error \"Transient\" is system error----------------");
                Console.WriteLine("SoapException Message= " + ex.Message);
                Console.WriteLine("");
                Console.WriteLine("SoapException Category:Code:Message= " + ex.Detail.LastChild.InnerText);
                Console.WriteLine("");
                Console.WriteLine("SoapException XML String for all= " + ex.Detail.LastChild.OuterXml);
                Console.WriteLine("");
                Console.WriteLine("SoapException StackTrace= " + ex.StackTrace);
                Console.WriteLine("-------------------------");
                Console.WriteLine("");
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                Console.WriteLine("");
                Console.WriteLine("--------------------");
                Console.WriteLine("CommunicationException= " + ex.Message);
                Console.WriteLine("CommunicationException-StackTrace= " + ex.StackTrace);
                Console.WriteLine("-------------------------");
                Console.WriteLine("");

            }
            catch (Exception ex)
            {
                Console.WriteLine("");
                Console.WriteLine("-------------------------");
                Console.WriteLine(" Generaal Exception= " + ex.Message);
                Console.WriteLine(" Generaal Exception-StackTrace= " + ex.StackTrace);
                Console.WriteLine("-------------------------");

            }
            finally
            {
                Console.ReadKey();
            }

           
                //XMLDocument

            return View();
        }
       
      
        public ActionResult AddressValidationTest() {
            XAVService xavSvc = new XAVService();
            XAVRequest xavRequest = new XAVRequest();

            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = "1CEB3632E5A4A196";
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = "crazy8monkey";
            upssUsrNameToken.Password = "Crazy3@monkey";
            upss.UsernameToken = upssUsrNameToken;
            xavSvc.UPSSecurityValue = upss;

            UPSRequestType request = new UPSRequestType();

            //Below code contains dummy data for reference. Please update as required.
            String[] requestOption = { "1" };
            request.RequestOption = requestOption;
            xavRequest.Request = request;
            AddressKeyFormatType addressKeyFormat = new AddressKeyFormatType();
            String[] addressLine = { "8514 LAFAYETTE AVE" };
            //addressKeyFormat.ItemsElementName = new ItemsChoiceType[] { ItemsChoiceType.PoliticalDivision1,ItemsChoiceType.PoliticalDivision2,ItemsChoiceType.PostcodePrimaryLow };
            String[] addressKeyFormatItems = { "NE", "Omaha", "68114" };
            //addressKeyFormat.Items = addressKeyFormatItems;
            addressKeyFormat.AddressLine = addressLine;
            addressKeyFormat.Urbanization = "OMAHA NE 68114";
            addressKeyFormat.ConsigneeName = "Adam Schmidt";
            addressKeyFormat.CountryCode = "US";
            xavRequest.AddressKeyFormat = addressKeyFormat;
            System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
            XAVResponse xavResponse = xavSvc.ProcessXAV(xavRequest);
            //Console.WriteLine("Response Status Code " + xavResponse.Response.ResponseStatus.Code);
            //Console.WriteLine("Response Status Description " + xavResponse.Response.ResponseStatus.Description);
           // Console.ReadLine();

            ViewData["Response"] = String.Format("Response Status Code {0}: Response Status Description {1}", xavResponse.Response.ResponseStatus.Code, xavResponse.Response.ResponseStatus.Description);

            return View();
        }


    }
}
