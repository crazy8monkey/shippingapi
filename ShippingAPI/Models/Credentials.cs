﻿using System;

namespace ShippingAPI.Models
{
    public class Credentials
    {
        public string accountUserName { get; set; }
        public string accountPassword { get; set; }
        public string developerString { get; set; }
        public string shipperNumber { get; set; }
        public string developerPassword { get; set; }

    }
}
