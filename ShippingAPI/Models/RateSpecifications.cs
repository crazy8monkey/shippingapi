﻿using System;
namespace ShippingAPI.Models
{
    public class RateSpecifications
    {
        public Boolean useRetailRates { get; set; }
        public Boolean IsResidential { get; set; }
        public Boolean useNegotiatedRates { get; set; }
    }
}
