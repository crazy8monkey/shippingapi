﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ShippingAPI.Models
{
    public class FedexShipping {
        string name;
        string friendly;
    }

    public class ShippingMethods
    {

        public Dictionary<String, String> UPSMethods = new Dictionary<string, string>
        {
            { "01", "UPS Next Day Air" },
            { "02", "UPS Second Day Air" },
            { "03", "UPS Ground" },
            { "07", "UPS Worldwide Express" },
            { "08", "UPS Worldwide Expedited" },
            { "11", "UPS Standard" },
            { "12", "UPS Three-Day Select" },
            { "13", "UPS Next Day Air Saver" },
            { "14", "UPS Next Day Air Early A.M." },
            { "54", "UPS Worldwide Express Plus" },
            { "59", "UPS Second Day Air A.M." },
            { "65", "UPS Saver" },
            { "82", "UPS Today Standard" },
            { "83", "UPS Today Dedicated Courier" },
            { "84", "UPS Today Intercity" },
            { "85", "UPS Today Express" },
            { "86", "UPS Today Express Saver" },
            { "92", "UPS SurePost (USPS) < 1lb" },
            { "93", "UPS SurePost (USPS) > 1lb" },
            { "94", "UPS SurePost (USPS) BPM" },
            { "95", "UPS SurePost (USPS) Media" }
        };

        public Dictionary<int, string[]> FedexMethods = new Dictionary<int, string[]> {
            { 0, new string[] { "EUROPE_FIRST_INTERNATIONAL_PRIORITY", "FedEx Europe First International Priority" } },
            { 1, new string[] { "FEDEX_1_DAY_FREIGHT", "FedEx 1st Day Freight"} },
            { 2, new string[] { "FEDEX_2_DAY", "FedEx 2nd Day" } },
            { 3, new string[] { "FEDEX_2_DAY_AM", "FedEx 2nd Day A.M." } },
            { 4, new string[] { "FEDEX_2_DAY_FREIGHT", "FedEx 2nd Day Freight" } },
            { 5, new string[] { "FEDEX_3_DAY_FREIGHT", "Fedex 3rd Day Freight" } },
            { 6, new string[] { "FEDEX_DISTANCE_DEFERRED", "FedEx Distance Deferred" } },
            { 7, new string[] { "FEDEX_EXPRESS_SAVER", "FedEx Express Saver" } },
            { 8, new string[] { "FEDEX_FIRST_FREIGHT", "FedEx First Freight" } },
            { 9, new string[] { "FEDEX_FREIGHT_ECONOMY", "FedEx Freight Economy" } },
            { 10, new string[] { "FEDEX_FREIGHT_PRIORITY", "FedEx Freight Priority" } },
            { 11, new string[] { "FEDEX_GROUND", "FedEx Ground" } },
            { 12, new string[] { "FEDEX_NEXT_DAY_AFTERNOON", "FedEx Next Day Afternoon" } },
            { 13, new string[] { "FEDEX_NEXT_DAY_EARLY_MORNING", "FedEx Next Day Early Morning" } },
            { 14, new string[] { "FEDEX_NEXT_DAY_END_OF_DAY", "FedEx Next Day End of Day" } },
            { 15, new string[] { "FEDEX_NEXT_DAY_FREIGHT", "FedEx Next Day Freight" } },
            { 16, new string[] { "FEDEX_NEXT_DAY_MID_MORNING", "FedEx Next Day Mid Morning" } },
            { 17, new string[] { "FIRST_OVERNIGHT", "FedEx First Overnight" } },
            { 18, new string[] { "GROUND_HOME_DELIVERY", "FedEx Ground Residential" } },
            { 19, new string[] { "INTERNATIONAL_ECONOMY", "FedEx International Economy" } },
            { 20, new string[] { "INTERNATIONAL_ECONOMY_FREIGHT", "FedEx International Economy Freight" } },
            { 21, new string[] { "INTERNATIONAL_FIRST", "FedEx International First" } },
            { 22, new string[] { "INTERNATIONAL_PRIORITY", "FedEx International Priority" } },
            { 23, new string[] { "INTERNATIONAL_PRIORITY_EXPRESS", "FedEx International Priority Express" } },
            { 24, new string[] { "INTERNATIONAL_PRIORITY_FREIGHT", "FedEx International Priority Freight" } },
            { 25, new string[] { "PRIORITY_OVERNIGHT", "FedEx Priority Overnight" } },
            { 26, new string[] { "SAME_DAY", "FedEx Same Day" } },
            { 27, new string[] { "SAME_DAY_CITY", "FedEx Same Day City" } },
            { 28, new string[] { "SMART_POST", "FedEx Smart Post" } },
            { 29, new string[] { "STANDARD_OVERNIGHT", "FedEx Standard Overnight" } }
        };
    }

   


           
}
