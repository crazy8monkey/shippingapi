﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShippingAPI.Models
{
    public class AddressType
    {
        private string cityString;
        private string stateString;
        private string zipString;
        private string countryString;

        public List<String> address { get; set; }
        public string city { 
            get
            {
                if(this.cityString == null) {
                    throw new Exception("City is Required");
                }
                return this.cityString;
            }
            set {
                this.cityString = value;
            }
        }
        public string state { 
            get
            {
                if (this.stateString == null)
                {
                    throw new Exception("State is Required");
                }
                return this.stateString;
            }
            set 
            {
                this.stateString = value;
            }
        }
        public string zip { 
            get
            {
                if (this.zipString == null)
                {
                    throw new Exception("Zip is Required");
                }
                return this.zipString;
            }
            set 
            {
                this.zipString = value;
            }
        }
        public string country { 
            get
            {
                if (this.countryString == null)
                {
                    throw new Exception("Country Code is Required");
                }
                return this.countryString;
            }
            set 
            {
                this.countryString = value;
            }
        }
    }
}
