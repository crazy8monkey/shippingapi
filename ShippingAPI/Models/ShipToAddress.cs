﻿using System;
using System.Collections.Generic;

namespace ShippingAPI.Models
{
    public class ShipToAddress
    {
        public List<String> address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
    }
}
