﻿using System;
using System.Collections.Generic;

namespace ShippingAPI.Models
{
    public class CatchRateResponse
    {
        private AddressType shipperAddressField;

        public Credentials credentials { get; set; }
        public AddressType shipperAddress { get; set; }
        public AddressType shipFromAddress { get; set; }
        public AddressType shipToAddress { get; set; }
        public List<Package> packages { get; set; }
        public string requestOption { get; set; }
        public Boolean useRetailRates { get; set; }
    }

   
}
