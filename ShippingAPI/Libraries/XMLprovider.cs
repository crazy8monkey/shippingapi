﻿using System;
namespace ShippingAPI.Libraries
{
    public interface XMLprovider
    {
        byte[] BuildXMLRequest();
    }
}
