﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace ShippingAPI.Libraries
{
    public class CatchWebResponse
    {
        //private WebRequest request;
        private Boolean isJsonRequest { get; set; }
        private string requestString { get; set; }


        public void setWebRequest() {

            using (Stream receiveStream = System.Web.HttpContext.Current.Request.InputStream)
            {
                receiveStream.Position = 0;
                using (StreamReader readStream = new StreamReader(receiveStream, System.Web.HttpContext.Current.Request.ContentEncoding))
                {
                    requestString = readStream.ReadToEnd();
                }
            }

            //checking if it response is json format
            if (System.Web.HttpContext.Current.Request.ContentType == "application/json")
            {
                isJsonRequest = true;
            }
            else
            {
                isJsonRequest = false;
            }
        }

        public string getWebRequest() {
            return requestString;
        }

        public void setIsJsonRequest() {
            
        }

        public Boolean getIsJsonRequestValue() {
            return isJsonRequest;
        }

    }

}
