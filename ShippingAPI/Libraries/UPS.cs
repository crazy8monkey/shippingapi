﻿using System;
using System.Linq;
using ShippingAPI.UPSRate;
using ShippingAPI.Properties.UPS;
using System.Collections.Generic;
using ShippingAPI.Models;

namespace ShippingAPI.Libraries
{
    public class UPS
    {
        private string serviceCodeString { get; set; }


        private UPSSecurity upsSecurityObject { get; set; }


        //private RateService rate = new RateService();
        //private RateResponse rateResponse = new RateResponse();

        //private RateRequest rateRequest = new RateRequest();
        //private ShipFromType shipFromType = new ShipFromType();
        //private ShipToType shipToType = new ShipToType();
        //private ShipToAddressType shiptToAddressType = new ShipToAddressType();

        //private ShipmentType shipment = new ShipmentType();
        //private ShipperType shipper = new ShipperType();
        //private UPSRequestType request = new UPSRequestType();

        //private AddressType shipperAddress = new AddressType();
        //private AddressType shipFromAddress = new AddressType();

        //private RateCodeDescriptionType service = new RateCodeDescriptionType();

        //private PackageWeightType packageWeight = new PackageWeightType();


        /* adding package rates */

        private List<PackageType> packageArray = new List<PackageType>();
        //private PackageType packageType = new PackageType();

        private string _accessNumber;
        private string _userName;
        private string _password;
        private string _shipperNumber;


        public UPS(Credentials shipperCredentials) {
            if (shipperCredentials == null)
            {
                throw new Exception("Credentials need to be provided");
            }
            _accessNumber = shipperCredentials.developerString;
            _userName = shipperCredentials.accountUserName;
            _password = shipperCredentials.accountPassword;
            _shipperNumber = shipperCredentials.shipperNumber;

        }


        //private byte[] BuildXMLRequest()
        //{
        //    return
        //}

        private void setConnection()
        {
            UPSSecurity upss = new UPSSecurity();
            UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
            upssSvcAccessToken.AccessLicenseNumber = _accessNumber;
            upss.ServiceAccessToken = upssSvcAccessToken;
            UPSSecurityUsernameToken upssUsrNameToken = new UPSSecurityUsernameToken();
            upssUsrNameToken.Username = _userName;
            upssUsrNameToken.Password = _password;
            upss.UsernameToken = upssUsrNameToken;
            upsSecurityObject = upss;
        }


        public void generateRates(CatchRateResponse rateResponseObj) 
        {
            RateService rate = new RateService();
            RateRequest rateRequest = new RateRequest();
            setConnection();

            rate.UPSSecurityValue = upsSecurityObject;

            UPSRequestType request = new UPSRequestType();

            if (rateResponseObj.requestOption == null)
            {
                throw new Exception("Request Option is is Required");
            }

            String[] requestOption = { rateResponseObj.requestOption };
            request.RequestOption = requestOption;
            rateRequest.Request = request;
            ShipmentType shipment = new ShipmentType();
            ShipperType shipper = new ShipperType();
            shipper.ShipperNumber = _shipperNumber;

            if (rateResponseObj.shipperAddress.address == null)
            {
                throw new Exception("Shipper Address is Required");
            }

            ShippingAPI.UPSRate.AddressType shipperAddress = new ShippingAPI.UPSRate.AddressType();
            String[] shipperAddressLine = { String.Join(", ", rateResponseObj.shipperAddress.address.ToArray()) };
            shipperAddress.AddressLine = shipperAddressLine;
            shipperAddress.City = rateResponseObj.shipperAddress.city;
            shipperAddress.PostalCode = rateResponseObj.shipperAddress.zip;
            shipperAddress.StateProvinceCode = rateResponseObj.shipperAddress.state;
            shipperAddress.CountryCode = rateResponseObj.shipperAddress.country;
            shipperAddress.AddressLine = shipperAddressLine;
            shipper.Address = shipperAddress;
            shipment.Shipper = shipper;
           

            if (rateResponseObj.shipFromAddress.address == null)
            {
                throw new Exception("Ship From Address is Required");
            }

            ShipFromType shipFrom = new ShipFromType();
            ShippingAPI.UPSRate.AddressType shipFromAddress = new ShippingAPI.UPSRate.AddressType();
            String[] shipFromAddressLine = { String.Join(", ", rateResponseObj.shipFromAddress.address.ToArray()) };
            shipFromAddress.AddressLine = shipFromAddressLine;
            shipFromAddress.City = rateResponseObj.shipFromAddress.city;
            shipFromAddress.PostalCode = rateResponseObj.shipFromAddress.zip;
            shipFromAddress.StateProvinceCode = rateResponseObj.shipFromAddress.state;
            shipFromAddress.CountryCode = rateResponseObj.shipFromAddress.country;
            shipFrom.Address = shipFromAddress;
            shipment.ShipFrom = shipFrom;
          

            if (rateResponseObj.shipToAddress.address == null)
            {
                throw new Exception("Ship To Address is Required");
            }
            
            ShipToType shipTo = new ShipToType();
            ShipToAddressType shipToAddress = new ShipToAddressType();
            String[] addressLine1 = { String.Join(", ", rateResponseObj.shipToAddress.address.ToArray()) };
            shipToAddress.AddressLine = addressLine1;
            shipToAddress.City = rateResponseObj.shipToAddress.city;
            shipToAddress.PostalCode = rateResponseObj.shipToAddress.zip;
            shipToAddress.StateProvinceCode = rateResponseObj.shipToAddress.state;
            shipToAddress.CountryCode = rateResponseObj.shipToAddress.country;
            shipTo.Address = shipToAddress;
            shipment.ShipTo = shipTo;
           




            //Below code uses dummy date for reference. Please udpate as required.
            if(rateResponseObj.requestOption == "Rate") {
                RateCodeDescriptionType service = new RateCodeDescriptionType();
                service.Code = "02";
                shipment.Service = service;    
            }

            List<PackageType> pkgArray = new List<PackageType>();

            foreach(var packageSingle in rateResponseObj.packages) {
                //this needs to loop via package numbers
                RateCodeDescriptionType uom = new RateCodeDescriptionType();
                uom.Code = "LBS";
                uom.Description = "pounds";

                PackageWeightType packageWeight = new PackageWeightType();
                packageWeight.Weight = packageSingle.weight;
                packageWeight.UnitOfMeasurement = uom;

                RateCodeDescriptionType packType = new RateCodeDescriptionType();
                packType.Code = "21";

                PackageType package = new PackageType();
                package.PackageWeight = packageWeight;
                package.PackagingType = packType;
                pkgArray.Add(package);
            }

            //PackageType[] pkgArray = { package };
            //PackageType[] pkgArray = new PackageType[];



            shipment.Package = pkgArray.ToArray();
            rateRequest.Shipment = shipment;
            System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
            //Console.WriteLine(rateRequest);
            //RateResponse rateResponse = rate.ProcessRate(rateRequest);
            //Console.WriteLine("The transaction was a " + rateResponse.Response.ResponseStatus.Description);
            //Console.WriteLine("Total Shipment Charges " + rateResponse.RatedShipment[0].TotalCharges.MonetaryValue + rateResponse.RatedShipment[0].TotalCharges.CurrencyCode);
            //Console.ReadKey();
            //rateResponse.Response

            //rateResponse = rate.ProcessRate(rateRequest);

        }

        //public RateResponse getRateResponse() {
            //return rateResponse;
        //}


    }
}
