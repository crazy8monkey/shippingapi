﻿using System;
using System.Collections.Generic;
using ShippingAPI.FedExRate;
using ShippingAPI.Models;

namespace ShippingAPI.Responses
{
    public class FedExResponses
    {
        private ShippingMethods shipMethods = new ShippingMethods();

        public FedExResponses()
        {
        }

        public object RateJSONResponse(RateReply rate)
        {
            var rateResponseObject = new object();
            List<object> shippingMethodRates = new List<object>();


            foreach(var rateResponseSinlge in rate.RateReplyDetails) {
                var serviceTypeInt = Convert.ToInt32(rateResponseSinlge.ServiceType);

                object rates = new object();
                foreach (var shipmentDetail in rateResponseSinlge.RatedShipmentDetails)
                {
                    var rateDetail = shipmentDetail.ShipmentRateDetail;
                    rates = new
                    {
                        //totalBillingWeight = rateDetail.TotalBillingWeight.Value
                        totalBaseCharge = rateDetail.TotalBaseCharge.Amount,
                        totalFreightCharges = rateDetail.TotalFreightDiscounts.Amount,
                        totalCharges = rateDetail.TotalSurcharges.Amount,
                        currency = rateDetail.TotalSurcharges.Currency
                    };

                    //if (rateDetail.Surcharges != null)
                    //{
                        // Individual surcharge for each package
                    //    foreach (Surcharge surcharge in rateDetail.Surcharges)
                    //        Console.WriteLine(" {0} surcharge {1} {2}", surcharge.SurchargeType, surcharge.Amount.Amount, surcharge.Amount.Currency);
                    //}
                    //if (rateDetail.TotalNetCharge != null) Console.WriteLine("Total Net Charge: {0} {1}", rateDetail.TotalNetCharge.Amount, rateDetail.TotalNetCharge.Currency);

                    //ShowShipmentRateDetails(shipmentDetail);
                    //Console.WriteLine();
                }

                shippingMethodRates.Add(new
                {
                    method = shipMethods.FedexMethods[serviceTypeInt][1],
                    rates
                }); 
                //
            }

            rateResponseObject = new
            {
                response = "success",
                shippingMethods = shippingMethodRates,
                shippingRates = rate
            };
            return rateResponseObject;
        }
    }
}
