﻿using System;
using System.Collections.Generic;
using System.Web.Helpers;
using System.Xml.XPath;
using ShippingAPI.Models;
//using ShippingAPI.UPSRate;
using ShippingAPI.Requests;


namespace ShippingAPI.Responses
{
    public class UPSResponses
    {

        private ShippingMethods shipMethods = new ShippingMethods();
        private UPSRequest upsRequest = new UPSRequest();

        public object GrabCurrentWebRequest { get; set; }
        public CatchRateResponse rateResponse { get; set; }

        public UPSResponses()
        {
        }

        public void GrabRateResponse(CatchRateResponse rateRequestObj)
        {
            rateResponse = rateRequestObj;
        }

        public object RateJSONResponse()
        {
            upsRequest.GrabRateResponse(rateResponse);
            upsRequest.setConnection();
            upsRequest.GetApiCall();
            //upsReqest.
            var response = upsRequest.GetXmlResponse();
            var rateResponseObject = new object();

            var ratedShipment = response.Root.Elements("RatedShipment");
            List<object> shippingMethodRates = new List<object>();

            foreach (var rateNode in ratedShipment) {
                var name = rateNode.XPathSelectElement("Service/Code").Value;

                shippingMethodRates.Add(new
                {
                    friendlyName = shipMethods.UPSMethods[name],
                    transportationCharges = rateNode.XPathSelectElement("TotalCharges/MonetaryValue").Value,
                    serviceOptionCharges = rateNode.XPathSelectElement("ServiceOptionsCharges/MonetaryValue").Value,
                    totalCharges = rateNode.XPathSelectElement("TotalCharges/MonetaryValue").Value
                });
            }

            rateResponseObject = new
            {
                response = "success",
                shippingMethods = shippingMethodRates
            };

            return rateResponseObject;
        }
    }
}
