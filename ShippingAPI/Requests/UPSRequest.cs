﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using ShippingAPI.Models;

namespace ShippingAPI.Requests
{
    public class UPSRequest
    {
        private const string DEVELOPMENT_RATES_URL = "https://wwwcie.ups.com/ups.app/xml/Rate";
        private const string PRODUCTION_RATES_URL = "https://onlinetools.ups.com/ups.app/xml/Rate";

        private CatchRateResponse catchRateResponse = new CatchRateResponse();

        private string _accessNumber;
        private string _userName;
        private string _password;
        private string _shipperNumber;


        public string RequestType { get; set; }
        private XDocument XmlResponses = new XDocument();


        public void GrabRateResponse(CatchRateResponse rateRequestObj) {
            catchRateResponse = rateRequestObj;
        }

        public void setConnection() {
            if (catchRateResponse.credentials == null)
            {
                throw new Exception("Credentials need to be provided");
            }

            //Console.WriteLine("this is a test {0}", catchRateResponse.shipToAddress.city);
            _accessNumber = catchRateResponse.credentials.developerString;
            _userName = catchRateResponse.credentials.accountUserName;
            _password = catchRateResponse.credentials.accountPassword;
            _shipperNumber = catchRateResponse.credentials.shipperNumber;
        }

        public void setShipperAddress() {
            
        }

        public void GetApiCall() {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            var request = (HttpWebRequest)WebRequest.Create(DEVELOPMENT_RATES_URL);
            request.Method = "POST";
            request.Timeout = 10 * 1000;
            // Per the UPS documentation, the "ContentType" should be "application/x-www-form-urlencoded".
            // However, using "text/xml; encoding=UTF-8" lets us avoid converting the byte array returned by
            // the buildRatesRequestMessage method and (so far) works just fine.
            request.ContentType = "text/xml; encoding=UTF-8"; //"application/x-www-form-urlencoded";
            var bytes = RateRequest();
            //System.Text.Encoding.Convert(Encoding.UTF8, Encoding.ASCII, this.buildRatesRequestMessage());
            request.ContentLength = bytes.Length;
            var stream = request.GetRequestStream();
            stream.Write(bytes, 0, bytes.Length);
            stream.Close();

            using (var resp = request.GetResponse() as HttpWebResponse)
            {
                if (resp != null && resp.StatusCode == HttpStatusCode.OK)
                {
                    var xDoc = XDocument.Load(resp.GetResponseStream());
                    SetXmlResponse(xDoc);
                }
            }
        }

        private byte[] RateRequest() {

            //Console.WriteLine("{0}", catRateResponse.credentials.accountUserName);
            
            Encoding utf8 = new UTF8Encoding(false);
            var writer = new XmlTextWriter(new MemoryStream(2000), utf8);
            writer.WriteStartDocument();
            writer.WriteStartElement("AccessRequest");
            writer.WriteAttributeString("lang", "en-US");
            writer.WriteElementString("AccessLicenseNumber", _accessNumber);
            writer.WriteElementString("UserId", _userName);
            writer.WriteElementString("Password", _password);
            writer.WriteEndDocument();
            writer.WriteStartDocument();
            writer.WriteStartElement("RatingServiceSelectionRequest");
            writer.WriteAttributeString("lang", "en-US");
            writer.WriteStartElement("Request");
            writer.WriteStartElement("TransactionReference");
            writer.WriteElementString("CustomerContext", "Rating and Service");
            writer.WriteElementString("XpciVersion", "1.0");
            writer.WriteEndElement(); // </TransactionReference>
            writer.WriteElementString("RequestAction", "Rate");
            writer.WriteElementString("RequestOption", "Shop");
            //writer.WriteElementString("RequestOption", string.IsNullOrWhiteSpace(_serviceDescription) ? "Shop" : _serviceDescription);
            writer.WriteEndElement(); // </Request>
            writer.WriteStartElement("PickupType");
            writer.WriteElementString("Code", "03");
            writer.WriteEndElement(); // </PickupType>
            writer.WriteStartElement("CustomerClassification");

            if (catchRateResponse.useRetailRates)
            {
                writer.WriteElementString("Code", "04"); //04 gets retail rates
            }
            else
            {
                writer.WriteElementString("Code", string.IsNullOrWhiteSpace(_shipperNumber) ? "01" : "00"); // 00 gets shipper number rates, 01 for daily rates
            }

            writer.WriteEndElement(); // </CustomerClassification
            writer.WriteStartElement("Shipment");
            writer.WriteStartElement("Shipper");
            writer.WriteElementString("ShipperNumber", _shipperNumber);
            writer.WriteStartElement("Address");
            writer.WriteElementString("PostalCode", catchRateResponse.shipperAddress.zip);
            writer.WriteElementString("CountryCode", catchRateResponse.shipperAddress.country);
            writer.WriteEndElement(); // </Address>
            writer.WriteEndElement(); // </Shipper>
            writer.WriteStartElement("ShipTo");
            writer.WriteStartElement("Address");
            writer.WriteElementString("StateProvinceCode", "CA");
            writer.WriteElementString("PostalCode", "92262");
            writer.WriteElementString("CountryCode", "US");
            //if (Shipment.DestinationAddress.IsResidential)
            //{
            //    writer.WriteElementString("ResidentialAddressIndicator", "true");
            //}
            writer.WriteEndElement(); // </Address>
            writer.WriteEndElement(); // </ShipTo>
            //if (!string.IsNullOrWhiteSpace(_serviceDescription))
            //{
            //    writer.WriteStartElement("Service");
            //    writer.WriteElementString("Code", _serviceDescription.ToUpsShipCode());
            //    writer.WriteEndElement(); //</Service>
            //}
            //if (_useNegotiatedRates)
            //{
            //    writer.WriteStartElement("RateInformation");
            //    writer.WriteElementString("NegotiatedRatesIndicator", "");
            //    writer.WriteEndElement();// </RateInformation>
            //}

            foreach (var packageSingle in catchRateResponse.packages)
            {
                writer.WriteStartElement("Package");
                writer.WriteStartElement("PackagingType");
                writer.WriteElementString("Code", "02");
                writer.WriteEndElement(); //</PackagingType>
                writer.WriteStartElement("PackageWeight");
                writer.WriteElementString("Weight", packageSingle.weight);
                writer.WriteEndElement(); // </PackageWeight>
                writer.WriteStartElement("Dimensions");
                writer.WriteElementString("Length","5");
                writer.WriteElementString("Width", "5");
                writer.WriteElementString("Height", "5");
                writer.WriteEndElement(); // </Dimensions>
                writer.WriteStartElement("PackageServiceOptions");
                writer.WriteStartElement("InsuredValue");
                writer.WriteElementString("CurrencyCode", "USD");
                //writer.WriteElementString("MonetaryValue", Shipment.Packages[i].InsuredValue.ToString());
                writer.WriteElementString("MonetaryValue", "50");
                writer.WriteEndElement(); // </InsuredValue>

                //if (Shipment.Packages[i].SignatureRequiredOnDelivery)
                //{
                //    writer.WriteStartElement("DeliveryConfirmation");
                //    writer.WriteElementString("DCISType", "2");         // 2 represents Delivery Confirmation Signature Required
                //    writer.WriteEndElement(); // </DeliveryConfirmation>
                //}

                writer.WriteEndElement(); // </PackageServiceOptions>
                writer.WriteEndElement(); // </Package>
            }
            writer.WriteEndDocument(); // </Shipment>
            writer.Flush();
            var buffer = new byte[writer.BaseStream.Length];
            writer.BaseStream.Position = 0;
            writer.BaseStream.Read(buffer, 0, buffer.Length);
            writer.Close();

            return buffer;
        }


        private void SetXmlResponse(XDocument xDoc) {
            XmlResponses = xDoc;
        }

        public XDocument GetXmlResponse() {
            return XmlResponses;
        }
    }
}
