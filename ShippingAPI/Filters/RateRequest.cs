﻿using System;
using System.Web.Mvc;
using Newtonsoft.Json;
using ShippingAPI.Libraries;
using ShippingAPI.Models;

namespace ShippingAPI.Filters
{
    public class RateRequest: ActionFilterAttribute, IActionFilter
    {
        private CatchWebResponse webResponse = new CatchWebResponse();

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext) {
            webResponse.setWebRequest();
            var currentWebRequest = webResponse.getWebRequest().GetType();
            var myRateResponse = JsonConvert.DeserializeObject<CatchRateResponse>(webResponse.getWebRequest());

            //checking
            //if(currentWebRequest.GetProperty("credentials") == null) {
            //    throw new Exception("Credentials need to be provided");
            //}

            //if ((myRateResponse.credentials.accountUserName == null))
            //{
            //    throw new Exception("Credentials: username need to be provided");
            //}

            //public string accountUserName { get; set; }
            //public string accountPassword { get; set; }
            //public string developerString { get; set; }
            //public string shipperNumber { get; set; }


            this.OnActionExecuting(filterContext);
        }
    }
}
