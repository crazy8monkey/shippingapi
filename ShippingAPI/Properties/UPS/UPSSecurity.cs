﻿using System;
using ShippingAPI.Properties;

namespace ShippingAPI.Properties.UPS
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.1433")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0", IsNullable = false)]
    public class UPSSecurity : System.Web.Services.Protocols.SoapHeader
    {

        private UPSSecurityUsernameToken usernameTokenField;

        private UPSSecurityServiceAccessToken serviceAccessTokenField;

        /// <remarks/>
        public UPSSecurityUsernameToken UsernameToken
        {
            get
            {
                return this.usernameTokenField;
            }
            set
            {
                this.usernameTokenField = value;
            }
        }

        /// <remarks/>
        public UPSSecurityServiceAccessToken ServiceAccessToken
        {
            get
            {
                return this.serviceAccessTokenField;
            }
            set
            {
                this.serviceAccessTokenField = value;
            }
        }
    }
}
