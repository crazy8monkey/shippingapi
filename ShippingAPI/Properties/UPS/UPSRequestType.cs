﻿using System;

namespace ShippingAPI.Properties.UPS
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4009")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0")]
    public class UPSRequestType
    {

        private string[] requestOptionField;

        private UPSTransactionReferenceType transactionReferenceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RequestOption")]
        public string[] RequestOption
        {
            get
            {
                return this.requestOptionField;
            }
            set
            {
                this.requestOptionField = value;
            }
        }

        /// <remarks/>
        public UPSTransactionReferenceType TransactionReference
        {
            get
            {
                return this.transactionReferenceField;
            }
            set
            {
                this.transactionReferenceField = value;
            }
        }
    }    

}
