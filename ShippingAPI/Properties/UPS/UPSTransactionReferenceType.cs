﻿using System;
namespace ShippingAPI.Properties.UPS
{

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.4009")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.ups.com/XMLSchema/XOLTWS/Common/v1.0")]
    public class UPSTransactionReferenceType
    {

        private string customerContextField;

        private string transactionIdentifierField;

        /// <remarks/>
        public string CustomerContext
        {
            get
            {
                return this.customerContextField;
            }
            set
            {
                this.customerContextField = value;
            }
        }

        /// <remarks/>
        public string TransactionIdentifier
        {
            get
            {
                return this.transactionIdentifierField;
            }
            set
            {
                this.transactionIdentifierField = value;
            }
        }
    }


}
