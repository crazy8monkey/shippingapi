﻿using System;
namespace ShippingAPI.Properties.UPS
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.1433")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0")]
    public class UPSSecurityServiceAccessToken
    {

        private string accessLicenseNumberField;

        /// <remarks/>
        public string AccessLicenseNumber
        {
            get
            {
                return this.accessLicenseNumberField;
            }
            set
            {
                this.accessLicenseNumberField = value;
            }
        }
    }
}
